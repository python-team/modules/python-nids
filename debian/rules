#!/usr/bin/make -f
# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1
include /usr/share/dpatch/dpatch.make

PYVERS=$(shell pyversions -r)

PACKAGE = python-nids
PREFIX := debian/$(PACKAGE)/usr

CFLAGS+= -lnids

build: patch-stamp
	for python in $(PYVERS); do \
	    CFLAGS="$(CFLAGS)" $$python setup.py build; \
	done

clean: unpatch
	dh_testdir
	
	# Add here commands to clean up after the build process.
	for python in $(PYVERS); do \
	   $$python setup.py clean; \
	done
	
	rm -f build-stamp
	rm -rf build
	
	dh_clean

install: build
	dh_testdir
	dh_testroot
	dh_installdirs
	
	for python in $(PYVERS); do \
	    $$python setup.py install --prefix=/usr --skip-build --root=$(CURDIR)/debian/$(PACKAGE); \
	done

get-orig-source:
	cd ..; wget http://jon.oberheide.org/pynids/downloads/pynids-0.6.1.tar.gz
	tar zxf ../pynids-0.6.1.tar.gz
	rm ../pynids-0.6.1.tar.gz
	rm pynids-0.6.1/libnids*.tar.gz
	mv pynids-0.6.1 pynids-0.6.1.orig
	tar -czf ../python-nids_0.6.1.orig.tar.gz pynids-0.6.1.orig
	rm -fr pynids-0.6.1.orig

binary-indep: build install

binary-arch: build install
	dh_testdir
	dh_testroot
	dh_installchangelogs CHANGES
	dh_installdocs README
	dh_installexamples Example
	dh_strip
	dh_pysupport
	dh_compress
	dh_fixperms
	dh_installdeb
	dh_shlibdeps
	dh_gencontrol
	dh_md5sums
	dh_builddeb

binary: binary-indep binary-arch
.PHONY: build clean binary-indep binary-arch binary install
